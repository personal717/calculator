import React from "react";
import './Button.css';

const Button = ({ data }) => {
  return (
    <button
      className={`button ${data.darker ? "darker" : ""} ${
        data.large ? "large" : ""
      }`}
      onClick={() => data.clickHandler()}
    >
      {data.label}
    </button>
  );
};

export default Button;
