import React, { useState, useEffect } from "react";
import "./App.css";
import Button from "./Button";

export default function App() {
  const [calcData, setCalcData] = useState({
    firstNum: "0",
    symbol: "",
    secondNum: "",
  });
  const [displayData, setDisplayData] = useState("");
  const [currentNum, setCurrentNum] = useState("firstNum");

  useEffect(() => {
    const newDisplayData = `${calcData.firstNum} ${transformSymbol(
      calcData.symbol
    )} ${calcData.secondNum}`;
    setDisplayData(newDisplayData);

    if (calcData.symbol) {
      setCurrentNum("secondNum");
    } else {
      setCurrentNum("firstNum");
    }
  }, [calcData]);

  function transformSymbol(symbol) {
    if (symbol === "*") {
      return "x";
    }
    if (symbol === "/") {
      return "÷";
    }
    return symbol;
  }

  function setState(setterFunc, field, value) {
    setterFunc((prev) => ({
      ...prev,
      [field]: value,
    }));
  }

  const setCalcState = setState.bind(undefined, setCalcData);

  function addChar(num) {
    const currentChar = String(calcData[currentNum]);
    const newNum =
      currentChar == "0"
        ? num
        : currentChar.includes(".") && num === "."
        ? currentChar
        : currentChar + String(num);
    setCalcState(currentNum, newNum);
  }

  function processPercentValue() {
    const result = Number(calcData[currentNum]) / 100;
    setCalcState(currentNum, result);
  }

  function addSymbol(symbol) {
    if (currentNum === "secondNum") {
      calculate(symbol);
    } else if (currentNum === "firstNum") {
      setCalcState("symbol", symbol);
    }
  }

  function clearCalc() {
    setCalcData({
      firstNum: "0",
      symbol: "",
      secondNum: "",
    });
  }

  function calculate(symbol = "") {
    if (calcData.firstNum && calcData.symbol && calcData.secondNum) {
      // Usually I would not use eval because of security reasons,
      // but for this example it is acceptable as we are avoiding a lot
      // of boilerplate code and the user cannot input anything in these fields
      const result = eval(
        calcData.firstNum + calcData.symbol + calcData.secondNum
      );
      setCalcData({
        firstNum: result,
        symbol,
        secondNum: "",
      });
    }
  }

  function changePositivity() {
    setCalcState(currentNum, calcData[currentNum] * -1);
  }

  const buttons = [
    {
      label: "AC",
      darker: true,
      clickHandler: clearCalc,
    },
    {
      label: "+/-",
      darker: true,
      clickHandler: changePositivity,
    },
    {
      label: "%",
      darker: true,
      clickHandler: processPercentValue,
    },
    {
      label: "÷",
      darker: true,
      clickHandler: addSymbol.bind(undefined, "/"),
    },
    {
      label: "7",
      clickHandler: addChar.bind(undefined, 7),
    },
    {
      label: "8",
      clickHandler: addChar.bind(undefined, 8),
    },
    {
      label: "9",
      clickHandler: addChar.bind(undefined, 9),
    },
    {
      label: "x",
      darker: true,
      clickHandler: addSymbol.bind(undefined, "*"),
    },
    {
      label: "4",
      clickHandler: addChar.bind(undefined, 4),
    },
    {
      label: "5",
      clickHandler: addChar.bind(undefined, 5),
    },
    {
      label: "6",
      clickHandler: addChar.bind(undefined, 6),
    },
    {
      label: "-",
      darker: true,
      clickHandler: addSymbol.bind(undefined, "-"),
    },
    {
      label: "1",
      clickHandler: addChar.bind(undefined, 1),
    },
    {
      label: "2",
      clickHandler: addChar.bind(undefined, 2),
    },
    {
      label: "3",
      clickHandler: addChar.bind(undefined, 3),
    },
    {
      label: "+",
      darker: true,
      clickHandler: addSymbol.bind(undefined, "+"),
    },
    {
      label: "0",
      large: true,
      clickHandler: addChar.bind(undefined, 0),
    },
    {
      label: ",",
      clickHandler: addChar.bind(undefined, "."),
    },
    {
      label: "=",
      darker: true,
      clickHandler: calculate,
    },
  ];

  return (
    <div className="calculator">
      <div className="calculator__wrapper">
        <div className="calculator__body">
          <span>{displayData}</span>
        </div>
        <div className="calculator__buttons">
          {buttons.map((btn, i) => (
            <Button data={btn} key={i} />
          ))}
        </div>
      </div>
    </div>
  );
}
